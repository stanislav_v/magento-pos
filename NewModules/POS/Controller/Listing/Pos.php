<?php
    namespace NewModules\POS\Controller\Listing;

    class Pos extends \Magento\Framework\App\Action\Action
    {
        protected $_pageFactory;

        public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $pageFactory
        )
        {
            $this->_pageFactory = $pageFactory;
            return parent::__construct($context);
        }

        public function execute()
        {
            $page = $this->_pageFactory->create();

            $id = null;
            $idTemp= $this->getRequest()->getParam('posorder');
            // $this->getRequest()->getParams();
            if (!is_null($idTemp))
                $id = (int) $idTemp;

            $layout = $page -> getLayout();
            $block = $layout -> getBlock('pos_listing_pos');
            $block -> setData('posorder', $id);

            return $page;
        }
    }