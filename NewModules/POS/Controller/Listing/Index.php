<?php
    namespace NewModules\POS\Controller\Listing;

    class Index extends \Magento\Framework\App\Action\Action
    {
        protected $_pageFactory;
        protected $_posFactory;

        public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $pageFactory,
            \NewModules\POS\Model\PosFactory $posFactory)
        {
            $this->_pageFactory = $pageFactory;
            $this->_posFactory = $posFactory;
            return parent::__construct($context);
        }

        public function execute()
        {
            $pos = $this->_posFactory->create();
            /* $collection = $pos->getCollection();
            foreach($collection as $item){
                echo "<pre>";
                print_r($item->getData());
                echo "</pre>";
            }
            exit(); */
            return $this->_pageFactory->create();
        }
    }