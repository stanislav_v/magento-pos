<?php

    namespace NewModules\POS\Controller\Adminhtml\Pos;

    use Magento\Backend\App\Action;
    use Magento\Backend\App\Action\Context;
    use Magento\Framework\View\Result\Page;
    use Magento\Framework\View\Result\PageFactory;
    use Magento\Framework\App\Action\HttpGetActionInterface;

    class Index extends Action implements HttpGetActionInterface
    {
        private $pageFactory;

        public function __construct(
            Context $context,
            PageFactory $pageFactory
        ) {
            $this->pageFactory = $pageFactory;

            parent::__construct($context);
        }

        public function execute(): Page
        {
            $resultPage = $this->pageFactory->create();
            // $resultPage->setActiveMenu('Magento_Catalog::catalog_products');
            $resultPage->getConfig()->getTitle()->prepend(__('POSes'));

            return $resultPage;
        }
    }