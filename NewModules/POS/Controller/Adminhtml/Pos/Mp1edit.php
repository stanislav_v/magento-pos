<?php

namespace NewModules\POS\Controller\Adminhtml\Pos;

use Magento\Backend\App\Action;

class Mp1edit extends \Magento\Backend\App\Action
{
	protected $_coreRegistry = null;

	protected $resultPageFactory;

	public function __construct(Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Registry $registry)
	{
		$this->resultPageFactory = $resultPageFactory;
		$this->_coreRegistry = $registry;
		parent::__construct($context);
	}

	protected function _isAllowed()
	{
		// return $this->_authorization->isAllowed(‘Jute_Ecommerce::save’);
		return true;
	}

	protected function _initAction()
	{
		// load layout, set active menu and breadcrumbs
		/** @var \Magento\Backend\Model\View\Result\Page $resultPage */
		$resultPage = $this->resultPageFactory->create();
		/* $resultPage->setActiveMenu(
			‘Jute_Ecommerce::ecommerce_manage’
		)->addBreadcrumb(
			__(‘Ecommerce’),
			__(‘Ecommerce’)
		)->addBreadcrumb(
			__(‘Manage Ecommerce’),
			__(‘Manage Ecommerce’)
		); */
		return $resultPage;
	}

	public function execute()
	{
		// 1. Get ID and create model
		$id = $this->getRequest()->getParam('pos_id'); //Primary key column
		$model = $this->_objectManager->create('NewModules\POS\Model\Pos');

		// 2. Initial checking
		if ($id) {
			$model->load($id);
			if (!$model->getId()) {
				$this->messageManager->addError(__('This POS no longer exists.'));
				/** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
				$resultRedirect = $this->resultRedirectFactory->create();
				return $resultRedirect->setPath('*/*/');
			}
		}

		// 3. Set entered data if was error when we do save
		$data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		// 4. Register model to use later in blocks
		$this->_coreRegistry->register('ecommerce', $model);

		// 5. Build edit form
		/** @var \Magento\Backend\Model\View\Result\Page $resultPage */
		$resultPage = $this->_initAction();
		/* $resultPage->addBreadcrumb(
			$id ? __(‘Edit Ecommerce’) : __(‘New Ecommerce’),
			$id ? __(‘Edit Ecommerce’) : __(‘New Ecommerce’)
		); */
		$resultPage->getConfig()->getTitle()->prepend(__('POS'));
		$resultPage->getConfig()->getTitle()
			->prepend($model->getId() ? $model->getTitle() : __('New POS'));
		return $resultPage;
	}
}