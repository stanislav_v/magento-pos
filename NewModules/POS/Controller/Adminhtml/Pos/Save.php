<?php

    namespace NewModules\POS\Controller\Adminhtml\Pos;

    class Save extends \Magento\Backend\App\Action
    {
        /**
         * @var \NewModules\POS\Model\PosFactory
         */
        var $gridFactory;

        /**
         * @param \Magento\Backend\App\Action\Context $context
         * @param \NewModules\POS\Model\PosFactory $posFactory
         */
        public function __construct(
            \Magento\Backend\App\Action\Context $context,
            \NewModules\POS\Model\PosFactory $posFactory
        ) {
            parent::__construct($context);
            $this->posFactory = $posFactory;
        }

        /**
         * @SuppressWarnings(PHPMD.CyclomaticComplexity)
         * @SuppressWarnings(PHPMD.NPathComplexity)
         */
        public function execute()
        {
            $data = $this->getRequest()->getPostValue();
            if (!$data) {
                $this->messageManager->addError(__('Internal error'));
                $this->_redirect('newmodules_pos/pos/edit');
                return;
            }
            try {
                $rowData = $this->posFactory->create();
                $rowData->setData($data);
                if (isset($data['pos_id'])) {
                    $rowData->setPosId($data['pos_id']);
                }
                $rowData->save();
                $this->messageManager->addSuccess(__('New POS has been successfully saved.'));
            } catch (\Exception $e) {
                $this->messageManager->addError(__($e->getMessage()));
            }
            $this->_redirect('newmodules_pos/pos/index');
        }

        /**
         * @return bool
         */
        protected function _isAllowed()
        {
            return $this->_authorization->isAllowed('Webkul_Grid::save');
        }
    }