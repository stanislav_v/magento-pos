<?php
    namespace NewModules\POS\Controller\Adminhtml\Pos;

    use Magento\Framework\Controller\ResultFactory;

    class Edit extends \Magento\Backend\App\Action
    {
        /**
         * @var \Magento\Framework\Registry
         */
        private $coreRegistry;

        /**
         * @var NewModules\POS\Model\PosFactory
         */
        private $posFactory;

        /**
         * @param \Magento\Backend\App\Action\Context $context
         * @param \Magento\Framework\Registry $coreRegistry,
         * @param \NewModules\POS\Model\PosFactory $gridFactory
         */
        public function __construct(
            \Magento\Backend\App\Action\Context $context,
            \Magento\Framework\Registry $coreRegistry,
            \NewModules\POS\Model\PosFactory $posFactory
        ) {
            parent::__construct($context);
            $this->coreRegistry = $coreRegistry;
            $this->posFactory = $posFactory;
        }

        /**
         * Mapped Grid List page.
         * @return \Magento\Backend\Model\View\Result\Page
         */
        public function execute()
        {
            $rowId = (int) $this->getRequest()->getParam('id');
            $rowData = $this->posFactory->create();

            if ($rowId) {
                $rowData = $rowData->load($rowId);
                $rowTitle = $rowData->getName();
                if (!$rowData->getPosId()) {
                    $this->messageManager->addError(__('rPOS no longer exist.'));
                    $this->_redirect('newmodules_pos/pos/edit');
                    return;
                }
            }

            $this->coreRegistry->register('row_data', $rowData);
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $title = $rowId ? __('Edit POS ').$rowTitle : __('Add POS');
            $resultPage->getConfig()->getTitle()->prepend($title);
            return $resultPage;
        }

        protected function _isAllowed()
        {
            return true;
        }
    }