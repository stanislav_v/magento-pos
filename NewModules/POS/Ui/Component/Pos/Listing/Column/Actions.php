<?php

namespace NewModules\POS\Ui\Component\Pos\Listing\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\Url;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class Actions extends Column
{
    const ROW_EDIT_URL = 'gridA/gridb/addrow';
    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;
    protected $_adminUrlBuilder;

    /**
     * @var string
     */
    protected $_viewUrl;
    protected $_editUrl;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Url $urlBuilder
     * @param string $viewUrl
     * @param array $components
     * @param array $data
     * @param UrlInterface $adminUrlBuilder;
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Url $urlBuilder,
        $viewUrl = '',
        array $components = [],
        array $data = [],
        $editUrl = self::ROW_EDIT_URL,
        UrlInterface $adminUrlBuilder
    ) {
        $this->_urlBuilder = $urlBuilder;
        $this->_viewUrl    = $viewUrl;
        $this->_editUrl = $editUrl;
        $this->_adminUrlBuilder = $adminUrlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                if (isset($item['pos_id'])) {
                    $item[$name]['edit']   = [
                        'href'  => $this->_adminUrlBuilder->getUrl($this->_editUrl, ['id' => $item['pos_id']]),
                        'label' => __('Edit')
                    ];
                    $item[$name]['view']   = [
                        'href'  => $this->_urlBuilder->getUrl($this->_viewUrl, ['posorder' => $item['pos_id']]),
                        'target' => '_blank',
                        'label' => __('View on Frontend')
                    ];
                }
            }
        }
        return $dataSource;
    }
}
