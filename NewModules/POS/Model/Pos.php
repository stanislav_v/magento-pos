<?php
    namespace NewModules\POS\Model;

    class Pos extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
    {
        const CACHE_TAG = 'pos_entity';

        protected $_cacheTag = 'pos_entity';

        protected $_eventPrefix = 'pos_entity';

        protected function _construct()
        {
            $this->_init('NewModules\POS\Model\ResourceModel\Pos');
        }

        public function getIdentities()
        {
            return [self::CACHE_TAG . '_' . $this->getId()];
        }

        public function getDefaultValues()
        {
            $values = [];

            return $values;
        }

        public function getPosId()
        {
            return $this->getData('pos_id');
        }
    
        public function setPosId($posId)
        {
            return $this->setData('pos_id', $posId);
        }

        public function getName()
        {
            return $this->getData('name');
        }

    }