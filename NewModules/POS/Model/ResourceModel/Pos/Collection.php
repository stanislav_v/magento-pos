<?php
    namespace NewModules\POS\Model\ResourceModel\Pos;

    class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
    {
        protected $_idFieldName = 'pos_id';
        protected $_eventPrefix = 'pos_entity_pos_collection';
        protected $_eventObject = 'pos_collection';

        /**
         * Define resource model
         *
         * @return void
         */
        protected function _construct()
        {
            $this->_init('NewModules\POS\Model\Pos', 'NewModules\POS\Model\ResourceModel\Pos');
        }

    }
