<?php
    namespace NewModules\POS\Model\ResourceModel;


    class Pos extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
    {
        
        public function __construct(
            \Magento\Framework\Model\ResourceModel\Db\Context $context
        )
        {
            parent::__construct($context);
        }
        
        protected function _construct()
        {
            $this->_init('pos_entity', 'pos_id');
        }
        
    }