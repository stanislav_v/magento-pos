<?php
    namespace NewModules\POS\Block;
    class Pos extends \Magento\Framework\View\Element\Template
    {
        protected $_posFactory;

        public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \NewModules\POS\Model\PosFactory $posFactory)
        {
            $this->_posFactory = $posFactory;
            parent::__construct($context);
        }

        public function getPos(int $posId) {
            // $posModel = $this->_posFactory->create();
            // $pos = $posModel->load($posId);
            $posColl = $this->_posFactory->create()->getCollection();
            $posColl -> addFieldToFilter('pos_id', $posId);
            $pos = $posColl->getFirstItem();

            return $pos -> getData();
        }

        public function getCacheLifetime()
        {
            return null;
        }
    }