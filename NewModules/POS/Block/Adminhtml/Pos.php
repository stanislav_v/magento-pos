<?php
    namespace NewModules\POS\Block\Adminhtml;

    class Pos extends \Magento\Backend\Block\Widget\Grid\Container
    {

        protected function _construct()
        {
            $this->_controller = 'adminhtml_pos';
            $this->_blockGroup = 'NewModules_POS';
            $this->_headerText = __('POSes');
            $this->_addButtonLabel = __('Create New POS');
            parent::_construct();
        }
    }