<?php
    namespace NewModules\POS\Block;
    class Index extends \Magento\Framework\View\Element\Template
    {
        protected $_posFactory;

        public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \NewModules\POS\Model\PosFactory $posFactory)
        {
            $this->_posFactory = $posFactory;
            parent::__construct($context);
        }
    
        public function sayHello()
        {
            return __('Hello World');
        }

        public function listPOSes()
        {
            $pos = $this->_posFactory->create();
            $collection = $pos->getCollection();
            foreach($collection as $item){
                echo "<pre>";
                print_r($item->getData());
                echo "</pre>";
            }
        }

        public function getPOSCollection()
        {
            $pos = $this->_posFactory->create();
            return $pos->getCollection();
        }
    }