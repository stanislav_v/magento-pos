<?php
    namespace NewModules\POS\Setup;

    require_once "InstallData.php";

    use Magento\Framework\Setup\ModuleContextInterface;
    use Magento\Framework\Setup\ModuleDataSetupInterface;
    use Magento\Framework\Setup\UpgradeDataInterface;

    // /var/www/html/magento/vendor/magento/magento2-base/dev/tests/setup-integration/_files/Magento/TestSetupDeclarationModule3/Setup/UpgradeData.php
    // /var/www/html/magento/vendor/magento/magento2-base/dev/tests/setup-integration/_files/Magento/TestSetupDeclarationModule3/revisions/first_patch_revision/UpgradeData.php

    // protože už jsem modul instaloval dříve,
    // potřebuju upgrade, jinak stačí jenom InstallData
    // pro správné použití upgrade je potřeba otestovat verzi

    class UpgradeData extends InstallData implements UpgradeDataInterface
    {
        public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
        {
            parent::install($setup, $context);
        }
    }
