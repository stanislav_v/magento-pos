<?php
    namespace NewModules\POS\Setup;

    // /var/www/html/magento/dev/tests/setup-integration/_files/Magento/TestSetupDeclarationModule3/Setup/InstallData.php
    // https://www.magentoextensions.org/documentation/interface_magento_1_1_framework_1_1_d_b_1_1_adapter_1_1_adapter_interface.html#ac5980a78ac7b1d5b60d5f23970372454

    require_once "InstallSchema.php";

    use Magento\Framework\Setup\InstallDataInterface;
    use Magento\Framework\Setup\ModuleContextInterface;
    use Magento\Framework\Setup\ModuleDataSetupInterface;

    class InstallData implements InstallDataInterface
    {

        public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
        {
            $adapter = $setup->getConnection();
            $setup->startSetup();
            $data = [];
            for ($x = 0; $x <= 100; $x++)
            {
                $data[] = [
                    'Point Of Sale '.$x,
                    'Address '.$x,
                    ceil($x/2) === ($x/2)
                ];
            }
            $adapter->insertArray(InstallSchema::TABLE_NAME, ['name', 'address', 'is_available'], $data);
            $setup->endSetup();
        }
    }
