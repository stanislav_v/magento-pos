<?php
    namespace NewModules\POS\Setup;

    require_once "InstallSchema.php";

    use Magento\Framework\Setup\UpgradeSchemaInterface;
    use Magento\Framework\Setup\SchemaSetupInterface;
    use Magento\Framework\Setup\ModuleContextInterface;

    // protože už jsem modul instaloval dříve,
    // potřebuju upgrade, jinak stačí jenom InstallSchema
    // pro správné použití upgrade je potřeba otestovat verzi

    class UpgradeSchema extends InstallSchema implements UpgradeSchemaInterface
    {
        public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context )
        {
            parent::install($setup, $context);
        }
    }