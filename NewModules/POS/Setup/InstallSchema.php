<?php
    namespace NewModules\POS\Setup;

    class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
    {
        const TABLE_NAME = 'pos_entity';

        public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
        {
            $installer = $setup;
            $installer->startSetup();
            if (!$installer->tableExists(self::TABLE_NAME))
            {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable(self::TABLE_NAME)
                )
                    ->addColumn(
                        'pos_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true, // auto_increment, https://stackoverflow.com/questions/5341693/add-an-auto-increment-column-in-magento-setup-script-without-using-sql/11194426
                            'nullable' => false,
                            'primary'  => true,
                            'unsigned' => true,
                        ],
                        'POS ID'
                    )
                    ->addColumn(
                        'name',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        255,
                        ['nullable' => false],
                        'POS Name'
                    )
                    ->addColumn(
                        'address',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        \Magento\Framework\DB\Ddl\Table::DEFAULT_TEXT_SIZE,
                        ['nullable' => true],
                        'POS Address'
                    )
                    ->addColumn(
                        'is_available',
                        \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                        null,
                        ['nullable' => true],
                        'Is Available'
                    )
                    ->setComment('Points of Sale Table');
                $installer->getConnection()->createTable($table);
            }
            $installer->endSetup();
        }
    }