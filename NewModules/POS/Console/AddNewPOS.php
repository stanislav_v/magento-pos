<?php
namespace NewModules\POS\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Input\InputOption;

class AddNewPOS extends Command
{
   const NAME = 'name';
   const ADDRESS = 'address';
   const ISAVAILABLE = 'isAvailable';

   const PARAMS = [self::NAME, self::ADDRESS, self::ISAVAILABLE];

   protected $_posFactory;

   public function __construct(\NewModules\POS\Model\PosFactory $posFactory)
   {
      $this->_posFactory = $posFactory;
      return parent::__construct();
   }

   protected function configure()
   {
      $options = [
         new InputOption(
            self::NAME,
            null,
            InputOption::VALUE_REQUIRED,
            'Name'
         ),
         new InputOption(
               self::ADDRESS,
               null,
               InputOption::VALUE_REQUIRED,
               'Name'
         ),
         new InputOption(
               self::ISAVAILABLE,
               null,
               InputOption::VALUE_REQUIRED,
               'Name'
         )
      ];

      $this->setName('magexo:pos:add');
      $this->setDescription('Add new POS (Point of Sale)')->setDefinition($options);

      parent::configure();
   }

   protected function execute(InputInterface $input, OutputInterface $output)
   {
       $missingParams = [];
       $values = [];
       foreach(self::PARAMS as $param)
       	if ($value = $input->getOption($param))
         {
	         $values[$param] = $value;
         }
         else
         {
            $missingParams[] = $param;
         }
       if(count($missingParams) > 0)
       {
         $output->writeln('Following parameters are missing:');
	      $output->writeln(implode(', ', $missingParams));
       }
       else
       {
	      $output -> writeln('Creating new POS:');
            foreach(self::PARAMS as $param)
		         $output -> writeln($param . ': ' . $values[$param]);

            $data = [
               'name' => $values[self::NAME],
               'address' => $values[self::ADDRESS],
               'is_available' => ('true' === $values[self::ISAVAILABLE])
            ];

            $this->_posFactory->create()->setData($data)->save();
      }
   }
}
